const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let rolesValidos = {
    values: ['ADMIN_ROLE','USER_ROLE','ORG_ADMIN_ROLE'],
    message: '{VALUE} no es válido'
}
let userSchema = new Schema({
    nombre: {
        type: String,
        required: [true, 'Nombre requerido']
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'Correo requerido']
        
    },
    password: {
        type: String,
        required: [true, 'Password requerido']
    },
    role: {
        type: String,
        default: "USER_ROLE",
        enum: rolesValidos
    },
    estado: {
        type: Boolean,
        default: true
    },
    access : {
        type : [String]
    },
    crew: {
        type: String,
        required: [true, 'Crew requerido']
    },
    rolInterbank: {
        type: String,
        required: [true, 'Rol requerido']
    }
});

userSchema.plugin(uniqueValidator, {message: '{PATH} debe de ser unico'});
module.exports = mongoose.model('User', userSchema);