const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let updates_battery = new Schema({
    pending_approve: {
        type: Boolean,
        default: true
    },
    nombre_copy: { 
        type: String
    }
});

let configurationSchema = new Schema({
    updates: {
        type: Boolean,
        default: true
    },
    last_update: {
        type: Date
    },
    updates_battery : { type: [updates_battery] }
});

configurationSchema.plugin(uniqueValidator, {message: '{PATH} debe de ser unico'});
module.exports = mongoose.model('Configuration', configurationSchema);