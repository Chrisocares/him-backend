

const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const program = require('./program.js').schema;

let Schema = mongoose.Schema;

let tipoAplicacion = {
    values: ['1','2'],
    message: '{VALUE} no es válido'
}

let applicationSchema = new Schema({
    
    nombre: {
        type: String,
        required: [true, 'Nombre del aplicativo requerido']
    },

    objetivo: {
        type: String,
        required: [true, 'Objetivo requerido']
    },

    custodio: {
        type: String,
        required: [true, 'Custodio requerido']
    },

    tipo: {
        type: String,
        enum: tipoAplicacion
    },

    plataforma: {
        type: String,
        required: [true, 'Plataforma requerido']
    },

    identificador : {
        type: String,
        required: [true, 'Identificador requerido']
    },

    programas : { type: [program] }
});



applicationSchema.plugin(uniqueValidator, {message: '{PATH} debe de ser unico'});
module.exports = mongoose.model('Application', applicationSchema);

