
const express = require('express')
const Configuration = require('../model/configuration');
const _ = require('underscore');

const app = express();

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.get('/updates/:id', function (rq, rs) {
    let id = rq.params.id;
    Configuration.findById(id,'updates updates_battery last_update')
        .exec((err, configurations) => {
            if (err) {
                return rs.status(400)
                    .json({
                        ok: false,
                        err
                    })
            }
            Configuration.count({}, (err, count) => {
                rs.json({
                    ok: true,
                    configurations
                });
            });
        });
});

app.post('/updates/:id',function(rq,rs){
    let id = rq.params.id;
    let body = _.pick(rq.body,["updates"]);

    Configuration.findByIdAndUpdate(id,body,{new: true,runValidators: true},(err,configurationDB)=>{

        if(err){
            return rs.status(400)
              .json({
                  ok: false,
                  err
              })
        }

        rs.json({
            ok: true,
            configurations: configurationDB
        });


    });
});

app.post('/updates',function(rq,rs){
    
    let body = rq.body;

    let configuration = new Configuration({
        updates: body.updates,
        last_update: new Date(),
        updates_battery : body.updates_battery
    });
    
    
    configuration.save((err,configurationDB)=>{

        if(err){
            return rs.status(400)
              .json({
                  ok: false,
                  err
              })
        }

        rs.json({
            ok: true,
            configuration: configurationDB
        });

    })
    
});



module.exports = app;