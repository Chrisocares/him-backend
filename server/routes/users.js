const express = require('express')
const User = require('../model/users');
const _ = require('underscore');
const bcrypt = require('bcrypt');

const app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
});

app.get('/users/:id',function(rq,rs){

    let id = rq.params.id;
    User.findById(id,'nombre email role estado access crew rolInterbank')
        .exec((err,users)=>{
            if(err){
                return rs.status(400)
                  .json({
                      ok: false,
                      err
                  })
            }
            User.count({},(err,count)=>{
                rs.json({
                    ok: true,
                    users
                });
            });
        })


})

app.get('/users',function(rq,rs){

    User.find({},'nombre email role estado access crew rolInterbank')
        .exec((err,users)=>{
            if(err){
                return rs.status(400)
                  .json({
                      ok: false,
                      err
                  })
            }
            User.count({},(err,count)=>{
                rs.json({
                    ok: true,
                    users
                });
            });
        })


})

app.post('/users', function (req, res) {
    let body = req.body;
    let { nombre, email, role, password, estado, access, crew, rolInterbank} = body;
    let usuario = new User({
      nombre,
      email,
      password: bcrypt.hashSync(password, 10),
      role,
      estado,
      access,
      crew,
      rolInterbank
    });
  usuario.save((err, usuarioDB) => {
      if (err) {
        return res.status(400).json({
           ok: false,
           err,
        });
      }
      res.json({
            ok: true,
            usuario: usuarioDB
         });
      })
  });

app.post('/users/:id',function(rq,rs){
    let id = rq.params.id;
    let body = _.pick(rq.body,["nombre","email","img","role","estado"]);

    

    User.findByIdAndUpdate(id,body,{new: true,runValidators: true},(err,userDB)=>{

        if(err){
            return rs.status(400)
              .json({
                  ok: false,
                  err
              })
        }

        if(userDB){
            return rs.status(404)
              .json({
                  ok: false,
                  err:{
                      message: "User not found"
                  }
              })
        }

        rs.json({
            ok: true,
            user: userDB
        });


    });
})

app.delete('/users/:id',function(rq,rs){
    
    let id = rq.params.id;
    User.findByIdAndRemove(id,(err,userDB)=>{

        if(err){
            return rs.status(400)
              .json({
                  ok: false,
                  err
              })
        }


        if(!userDB){
            return rs.status(404)
              .json({
                  ok: false,
                  err:{
                      message: "User not found"
                  }
              })
        }

        rs.json({
            ok: true,
            user: userDB
        });


    });
})


module.exports = app;
