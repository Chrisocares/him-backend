
const express = require('express')
const Application = require('../model/application');
const _ = require('underscore');

const app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/apps',function(rq,rs){
    
    let page = rq.query.page||1;
    page = Number(page)-1;

    let size = rq.query.size||5;

    size = Number(size);

    Application.find({})
        .skip((page)*size)
        .limit(size)
        .exec((err,applications)=>{

            if(err){
                return rs.status(400)
                  .json({
                      ok: false,
                      err
                  })
            }

            Application.count({},(err,count)=>{

                rs.json({
                    ok: true,
                    applications,
                    count
                });

            });
            
        })
})


app.get('/AppById/:id',function(rq,rs){

    let id = rq.params.id;
    Application.findById(id)
        .exec((err,applications)=>{

            if(err){
                return rs.status(400)
                  .json({
                      ok: false,
                      err
                  })
            }

            Application.count({},(err,count)=>{

                rs.json({
                    applications
                });

            });
            
        })


})


app.post('/apps',function(rq,rs){
    
    let body = rq.body;

    let application = new Application({
        identificador: body.identificador,
        nombre: body.nombre,
        objetivo: body.objetivo,
        custodio: body.custodio,
        plataforma: body.plataforma,
        tipo: body.tipo,
        programas : body.programs
    });
    
    
    application.save((err,applicationDB)=>{

        if(err){
            return rs.status(400)
              .json({
                  ok: false,
                  err
              })
        }

        rs.json({
            ok: true,
            application: applicationDB
        });

    })
    
})

app.get('/apps/download', function (rq, rs) {
    rs.download('./RM-aplicacion.xlsx');    
});

module.exports = app;